# Mirror

Records information about the disk for possible data recovery.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MIRROR.LSM

<table>
<tr><td>title</td><td>Mirror</td></tr>
<tr><td>version</td><td>0.2a</td></tr>
<tr><td>entered&nbsp;date</td><td>1999-03-24</td></tr>
<tr><td>description</td><td>Records information about the disk for possible data recovery.</td></tr>
<tr><td>keywords</td><td>freedos</td></tr>
<tr><td>author</td><td>Brian E. Reifsnyder</td></tr>
<tr><td>maintained&nbsp;by</td><td>Brian E. Reifsnyder, reifsnyderb@mindspring.com</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Mirror</td></tr>
</table>
